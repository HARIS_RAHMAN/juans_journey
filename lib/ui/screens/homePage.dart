import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:juaans_journy/utils/contants.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {  
  double horizontal = SizeConfig.safeBlockHorizontal;
  double vertical = SizeConfig.safeBlockVertical;
  List topSlider = [
    'travel_to_explore.png',
    'travel_to_explore.png',
    'travel_to_explore.png',
  ];
  List popularResorts = [
    'popular_image1.png',
    'popular_image2.png',
    'popular_image3.png',
    'popular_image1.png'
  ];
  List recommendedResorts = [
    'popular_image2.png',
    'popular_image3.png',
    'popular_image1.png',
    'popular_image2.png',
  ];
  List bestResorts = [
    'popular_image3.png',
    'popular_image1.png',
    'popular_image2.png',
    'popular_image3.png',
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        title: Text(
          'JUAANS JOURNY',
          style: TextStyle(
              color: appBlue,
              fontSize: horizontal * 4,
              fontFamily: segoeSemibold),
        ),
        leading: IconButton(
          padding: EdgeInsets.zero,
          onPressed: () {},
          icon:
              Image.asset(imagePath + 'drawer_icon.png', height: vertical * 3),
        ),
        actions: [
          notificationButton(),
        ],
      ),
      body: ListView(
        children: [
          Container(
            height: vertical * 30,
            child: Stack(
              children: [
                Container(
                  height: vertical * 24,
                  alignment: Alignment.topCenter,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(30),
                          bottomRight: Radius.circular(30)),
                      boxShadow: [
                        BoxShadow(
                            color: Colors.black.withOpacity(0.16),
                            blurRadius: 10)
                      ]),
                  child: Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: horizontal * 5.5,
                        vertical: horizontal * 0.5),
                    child: Column(
                      children: [
                        Container(
                          decoration: BoxDecoration(
                              color: boxBlue,
                              borderRadius: BorderRadius.circular(10)),
                          child: Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: horizontal * 3,
                                vertical: horizontal * 3),
                            child: Column(
                              children: [
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          'Current Location',
                                          style: TextStyle(
                                              fontSize: horizontal * 2.9,
                                              fontFamily: segoeRegular),
                                        ),
                                        Text(
                                          'Wayanad, Kerala 34345',
                                          style: TextStyle(
                                              color: HexColor('#4B4B4B'),
                                              fontSize: horizontal * 2.8,
                                              fontFamily: segoeSemiLight),
                                        ),
                                      ],
                                    ),
                                    Spacer(),
                                    iconWithGreyCircle('gps_icon.png'),
                                    SizedBox(
                                      width: horizontal * 3,
                                    ),
                                    iconWithGreyCircle('location_icon.png'),
                                  ],
                                ),
                                SizedBox(
                                  height: vertical * 1,
                                ),
                                Row(
                                  children: [
                                    Expanded(
                                        child: Container(
                                      decoration: BoxDecoration(
                                          color: Colors.white,
                                          border: Border.all(
                                              color: HexColor('#A5A5A5'),
                                              width: 0.3),
                                          borderRadius:
                                              BorderRadius.circular(50)),
                                      child: TextField(
                                        decoration: InputDecoration(
                                            isDense: true,
                                            hintText: 'Search in your locality',
                                            hintStyle: TextStyle(
                                                fontSize: horizontal * 2.9,
                                                fontFamily: segoeSemiLight,
                                                color: HexColor('#4B4B4B')
                                                    .withOpacity(0.45)),
                                            suffixIcon: IconButton(
                                              padding: EdgeInsets.all(0),
                                              icon: Container(
                                                  padding: EdgeInsets.all(
                                                      vertical * 1),
                                                  decoration: BoxDecoration(
                                                      color: HexColor('#BBBBBB')
                                                          .withOpacity(0.26),
                                                      shape: BoxShape.circle),
                                                  child: Image.asset(
                                                    imagePath +
                                                        'search_icon.png',
                                                    height: vertical * 2,
                                                    width: vertical * 2,
                                                  )),
                                              onPressed: () {},
                                            ),
                                            contentPadding:
                                                EdgeInsets.symmetric(
                                                    horizontal: horizontal * 3,
                                                    vertical: horizontal * 3),
                                            border: InputBorder.none,
                                            enabledBorder: InputBorder.none,
                                            errorBorder: InputBorder.none,
                                            focusedBorder: InputBorder.none,
                                            disabledBorder: InputBorder.none),
                                      ),
                                    )),
                                    SizedBox(
                                      width: horizontal * 2.5,
                                    ),
                                    Container(
                                        padding: EdgeInsets.all(vertical * 1),
                                        decoration: BoxDecoration(
                                            color: HexColor('#BBBBBB')
                                                .withOpacity(0.26),
                                            shape: BoxShape.circle),
                                        child: Image.asset(
                                          imagePath + 'more_icon.png',
                                          height: vertical * 2.5,
                                          width: vertical * 2.5,
                                        )),
                                  ],
                                )
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      optionsContainer('story_icon.png', 'My Story'),
                      SizedBox(
                        width: horizontal * 2.5,
                      ),
                      optionsContainer('enquiry_icon.png', 'My Enquiry'),
                      SizedBox(
                        width: horizontal * 2.5,
                      ),
                      optionsContainer(
                          'booking_history_icon.png', 'Booking History'),
                      SizedBox(
                        width: horizontal * 2.5,
                      ),
                      optionsContainer('profile_item_icon.png', 'Profile'),
                    ],
                  ),
                )
              ],
            ),
          ),
          SizedBox(height: vertical * 2.5),
          CarouselSlider(
            options: CarouselOptions(
              height: vertical * 18,
            ),
            items: topSlider.map((i) {
              return Builder(
                builder: (BuildContext context) {
                  return Container(
                    width: MediaQuery.of(context).size.width,
                    margin: EdgeInsets.symmetric(horizontal: horizontal * 1),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        image: DecorationImage(
                            image: AssetImage(imagePath + '$i'))),
                  );
                },
              );
            }).toList(),
          ),
          SizedBox(height: vertical * 2.5),
          Padding(
            padding: EdgeInsets.only(left: horizontal * 4),
            child: Text('Are you looking for ?'),
          ),
          Padding(
            padding: EdgeInsets.only(left: horizontal * 4),
            child: Text('Popular Resorts'),
          ),
          SizedBox(height: vertical * 2),
          Container(
            height: 231,
            child: ListView.separated(
              padding: EdgeInsets.symmetric(horizontal: horizontal * 2),
              scrollDirection: Axis.horizontal,
              itemCount: popularResorts.length,
              itemBuilder: (context, index) {
                return Container(
                  height: 231,
                  width: 150,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      image: DecorationImage(
                          image:
                              AssetImage(imagePath + popularResorts[index]))),
                );
              },
              separatorBuilder: (context, index) {
                return SizedBox(width: horizontal * 2);
              },
            ),
          )
        ],
      ),
    );
  }

  Widget optionsContainer(String icon, String text) {
    return Container(
      alignment: Alignment.center,
      height: vertical * 12,
      width: vertical * 10,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(color: Colors.black.withOpacity(0.16), blurRadius: 9)
          ]),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            alignment: Alignment.center,
            decoration: BoxDecoration(color: boxBlue, shape: BoxShape.circle),
            child: Container(
              height: vertical * 3,
              decoration: BoxDecoration(
                  image: DecorationImage(image: AssetImage(imagePath + icon))),
            ),
          ),
          SizedBox(height: vertical * 0.8),
          Text(
            text,
            style:
                TextStyle(fontSize: horizontal * 1.9, fontFamily: segoeRegular),
          )
        ],
      ),
    );
  }

  Widget iconWithGreyCircle(String icon) {
    return Container(
        height: vertical * 4.5,
        padding: EdgeInsets.all(vertical * 0.8),
        decoration: BoxDecoration(
            color: HexColor('#BBBBBB').withOpacity(0.26),
            shape: BoxShape.circle),
        child: Image.asset(
          imagePath + icon,
          height: vertical * 3,
          width: vertical * 3,
        ));
  }

  Widget notificationButton() {
    return IconButton(
      padding: EdgeInsets.only(right: horizontal * 3.5),
      onPressed: () {},
      icon: Stack(
        children: [
          Container(
              padding: EdgeInsets.all(6),
              decoration: BoxDecoration(color: appBlue, shape: BoxShape.circle),
              child: Image.asset(imagePath + 'notification_icon.png',
                  height: vertical * 3)),
          Positioned.fill(
            right: vertical * 0.7,
            top: vertical * 0.6,
            child: Align(
              alignment: Alignment.topRight,
              child: Container(
                alignment: Alignment.center,
                height: vertical * 1.6,
                width: vertical * 1.6,
                decoration: BoxDecoration(
                    color: HexColor('#FF0000'),
                    shape: BoxShape.circle,
                    border: Border.all(color: Colors.white, width: 0.5)),
                child: Text(
                  '2',
                  style: TextStyle(fontSize: horizontal * 1.5),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
