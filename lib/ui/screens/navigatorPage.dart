import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:juaans_journy/ui/screens/homePage.dart';
import 'package:juaans_journy/utils/contants.dart';

class NavigatorPage extends StatefulWidget {
  @override
  _NavigatorPageState createState() => _NavigatorPageState();
}

class _NavigatorPageState extends State<NavigatorPage> {
  int _currentIndex = 0;
  static List<Widget> _widgetOptions = <Widget>[
    HomePage(),
    FlutterLogo(
      style: FlutterLogoStyle.markOnly,
    ),
    FlutterLogo(style: FlutterLogoStyle.stacked),
    HomePage()
  ];


  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      body: Center(
        child: _widgetOptions.elementAt(_currentIndex),
      ),
      bottomNavigationBar: BottomAppBar(
          color: Colors.transparent,
          elevation: 0,
          child: Container(
            height: SizeConfig.safeBlockVertical*6.5,
            decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                      color: Colors.black.withOpacity(0.16), blurRadius: 3)
                ],
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(12),
                    topRight: Radius.circular(12))),
            child: new Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                bottomBarItem('home_icon.png',0),
                bottomBarItem('search_icon.png',1),
                bottomBarItem('history_icon.png',2),
                bottomBarItem('profile_icon.png',3),
              ],
            ),
          )),
    );
  }

  IconButton bottomBarItem(String icon,int index) => IconButton(
        icon: Image.asset(
          imagePath + icon,
          height: SizeConfig.safeBlockVertical*3,
          color:  _currentIndex == index ? appBlue : HexColor('#0D1335'),
        ),
        onPressed: () {
          _currentIndex == index
          // ignore: unnecessary_statements
              ? null
              : setState(() => _currentIndex = index);
        },
      );
}
